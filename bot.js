const Twitter = require('twit');
const fs = require('fs');
const https = require('https');
const config = require('./config.js');
const images = require('./images.js');

const T = new Twitter(config);

function randomFromArray(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

async function tweetRandomImage() {
  const imageUrl = randomFromArray(images);
  console.log('fetching an image...', imageUrl);

  var download = function (url, dest, cb) {
    var file = fs.createWriteStream('file.jpg');
    var request = https.get(url, function (response) {
      response.pipe(file);
      file.on('finish', function () {
        file.close(() => {
          var b64content = fs.readFileSync('file.jpg', { encoding: 'base64' })

          console.log('uploading an image...');
          T.post('media/upload', { media_data: b64content }, function (err, data, response) {
            if (err) console.log(err)
            else {
              const image = data;
              console.log('image uploaded, adding description...');

              T.post('media/metadata/create', {
                media_id: image.media_id_string,
                alt_text: {
                  text: 'Gatitos chillando'
                }
              }, function (err, data, response) {
                T.post('statuses/update', {
                  status: `${data}`,
                  media_ids: [image.media_id_string]
                },
                  function (err, data, response) {
                    if (err) console.log('error2:', err);
                    else {
                      console.log('posted an image!');
                    }
                  });
              });
            }
          })
        });
      });
    }).on('error', function (err) {
      fs.unlink(dest);
      if (cb) cb(err.message);
    });
  };

  download(imageUrl)
}


tweetRandomImage();